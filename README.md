본 프로젝트는 Gitlab.com 사용법을 제공하는 프로젝트입니다.

* gitlab.com의 특징
  - 장점: github.com 대비 private repository를 무한정 만들 수 있다는 장점이 있음.
  - 단점: 좀 속도가 느리고, 자주 500 서버 에러가 난다.

* 준비과정

  (1) 웹 페이지에서 gitlab.com 을 로그인한다.

      google 계정을 이용하면, 별도의 계정 생성없이 로그인 할 수 있다.

  (2) 비밀번호와 SSH 키를 생성해야 리뉵스/PC 코맨드라인으로 프로젝트 리파지토리를 다운로드(pull)하거나 업로드(push)할 수 있다.

      - 비밀번호는 아무거나 8글자 이상
      - SSH 키 생성은 다음 명령어로 만든다.
        $ ssh-keygen -t rsa -b 4096 -C "my-email-id@gmail.com" 
        자기 리눅스 계정의 ~/.ssh/id_rsa.pub 폴더에 있는 텍스트를 SSH 키로 copy-paste 형태로 등록할 수 있다.

  (3) 프로젝트 생성은 자유롭게 한다.

      생성된 프로젝트는 git@gitlab.com:my-email-id/projectname.git 형태의 URL에 매핑된다.


  (3) 리눅스 계정에서 해당 프로젝트를 클로닝해 온다.

      $ git clone git@gitlab.com:my-email-id/projectname.git

      이렇게 하면, ./projectname 폴더가 생긴다.

  (4) 해당 폴더 속에 들어가서, 

      $ vi test.txt 로 파일을 하나 생 생성한 후에...
      $ git add test.txt    로 해당 파일을 기트가 인식할 수 있도록 만들고...
      $ git commit -am "test file 생성"   이런 식으로 만들어진 파일을 commit 한 후에... (아직은 리모트 서버와 상관없다.)
      $ git push -u origin master        마스터 리파지토리에 업로드(push) 한다.

      웹에 가서 해당 파일이 생성되었는지 확인한다.

  (5) 웹에서 test2.txt 라는 파일을 생성한다.

       $ git pull 명령어로 웹에서 생성한 파일이 내 로컬 리눅스로 복사되어 오는지 확인한다.

       $ vi test2.txt   로 파일을 약간 수정하고...
       $ git commit -am "test 2 update"   로 변경된 내용을  commit 한 후에...
       $ git push -u origin master      마스터 리파지토리에 업로드(push) 한다.

   (6) 리눅스 폴더에서 

       $ mkdir src
       $ mv test.txt test2.txt ./src/
       $ git add ./src 를 하면 ./src 폴더 하위에 있는 전부가 리파지토리로 편입된다.
       $ git commit -am "after file move" 로 하면 반영된다.
       $ git push -u origin master    마스터 리파지토리에 업로드(push)한다.

