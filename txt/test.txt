1. gitlab 사용법
2. git push -u origin master 이게 뭔 뜻인가?
3. pull request란 뭣인가?

src refspec master does not match any

최초:
  $ touch initial
  $ git add initial
  $ git commit -am "text"

  $ git push -u origin master

gitlab은 에러가 너무 많이 난다.


ssh-keygen -t rsa -b 4096 -C "hiongun@gmail.com"


$ git clone id@host:project.git
$ cd project/
$ vi xx.cpp
$ git commit -am 'test committed'
$ vi TODO
$ git commit -am 'added a new file'
$ git push origin master
$ git push origin master <--- rejected 

$ git fetch origin 후에 
$ git merge origin/master 한 후에...
$ git push origin master 를 한다.

issue54 브랜치를 만든다.

$ git log --no-merges origin/master ^issue54

$ git checkout master
$ git merge issue54
$ git merge origin/master


$ git diff --check  (공백문자를 체크한다)
$ git add --patch (한 부분씩 나눠어 Staging Area에 저장한다)
$ 

$ git checkout -b featureA
$ vi test.xx
$ git commit -am 'test git'
$ git push origin featureA

$ git fetch origin
$ git checkout -b featureB
$ vi test.xxx
$ git commit -am 'test'

$ git fetch origin

$ git merge origin/featureB

$ git push origin featureB:featureBee

$ git fetch origin
$ git log origin/featureA ^featureA

$ git checkout featureA
$ git merge origin/featureA

$ git commit -am 'small changes'

$ git push origin featureA

$ git clone (url)
$ cd project
$ git checkout -b featureA
$ (work)
$ git commit
$ (work)
$ git commit

$ git remote add myfork (url)

$ git push myfork featureA

$ git pull 원격지에 있는 것을 미러링 해 옴.

$ git request-pull origin/master fork

....

디렉토리 변경을 한 후에 그 결과를 반영하기...



...

$ git checkout -b featureB origin/master
$ (work)
$ git commit
$ git push myfork featureB
$ (email maintainer)
$ git fetch origin

$ git checkout featureA
$ git rebase origin/master
$ git push -f myfork featureA

-----------
1. fork는 남의 repository를 나의 repository로 복사해 오는 역할을 한다.
2. clone/remote 생성
   clone or download 링크를 찾아낸다.

3. git clone https://github.com/wayhome25/blog.github.io.git
   클로닝을 한다.

(원본 프로젝트 저장소를 원격 저장소로 추가)
4. git remote add real-blog(별명) https://github.com/원본계정/blog.github.io.git

5. git remote -v (원격 저장소 설정 현황 확인)

(브랜치 생성)
6. git checkout -b develop     (브랜치를 체크아웃한다.)

7. git branch   (존재하는 브랜치 목록을 본다)

(수정 작업후 add, commit, push 한다.)
8. git push origin develop

[compare pull request] 라는 링크가 생긴다.

Create Pullrequest를 생성한다.

----------

$ git pull real-blog  (remote별명)

$ git branch -d develop (브랜치명)

$ git pull real-blog (remote별명)
